/*
 * NEMA17.h
 *
 *  Created on: 30 ago 2021
 *      Author: lucad
 */

#ifndef INC_NEMA17_H_
#define INC_NEMA17_H_

#include <stdint.h>
#include "TMC2208.h"
#include <stdbool.h>
#include "main.h"

typedef struct
{
	uint32_t port;
	uint32_t pin;
} PIN;

typedef enum
{
	SENSE_CW,
	SENSE_CCW
} NEMA17_Sense;

typedef struct
{
	uint8_t EndStop_Click;
	bool enable;
} NEMA17_EndStop;

typedef struct
{
	uint32_t Steps;
	bool enable;
} NEMA17_Stop_At;

typedef struct
{
	bool is_enable;
	TMC2208 Driver;

	PIN Enable;
	PIN Dir_Pin;
	uint16_t EXTI_Int_Pin; // Per step
	uint16_t EXTI_Int_EndStop_Pin; // Per fine corsa

	NEMA17_EndStop Min_EndStop;
	NEMA17_Sense Direction;

	bool enable_step_counter;
	uint32_t Steps;
	NEMA17_Stop_At Step_Stop;

	uint32_t Velocita;

	TIM_HandleTypeDef * Timer; // usato per impostare le varie velocita
	bool Speed_By_Timer;
	uint32_t Timer_Max_Frequency;
	volatile uint32_t * CCR;

	TIMER work;
	bool Home;
} NEMA17;

void Nema17_Init(NEMA17 * nema, UART_HandleTypeDef * uart, uint32_t Enable_Pin_Port, uint32_t Enable_Pin, uint32_t Direction_Pin_Port, uint32_t Direction_Pin, uint16_t EXTI_Int_EndStop_Pin, TIM_HandleTypeDef * Timer, bool Speed_By_Timer, uint32_t Timer_Max_Frequency, uint32_t * CCR);
void Nema17_Start(NEMA17 * nema);
void Nema17_Invert_Direction(NEMA17 * nema);
void Nema17_Set_Speed(NEMA17 * nema, uint32_t velocita);
uint32_t Nema17_Get_Speed(NEMA17 * nema);
void Nema17_Enable(NEMA17 * nema);
void Nema17_Stall(NEMA17 * nema);
void Nema17_Release(NEMA17 * nema);
NEMA17_Sense Nema17_Get_Sense(NEMA17 * nema);
void Nema17_Increment_Steps(NEMA17 * nema, TIM_HandleTypeDef * Timer);
void Nema17_Direction_CCW(NEMA17 * nema);
void Nema17_Direction_CW(NEMA17 * nema);
bool Nema17_Get_Enable_Step_Counter(NEMA17 * nema);
void Nema17_Start_Homing(NEMA17 * nema);
void Nema17_Homing_Stage_1(NEMA17 * nema);
void Nema17_Homing_Stage_2(NEMA17 * nema);
void Nema17_Homing_Stage_3(NEMA17 * nema);
void Nema17_EndStop(NEMA17 * nema, uint16_t GPIO_Pin);
void Nema17_Stop_At_N_Step(NEMA17 * nema);
void Nema17_Setup_Steps_Stop(NEMA17 * nema, uint32_t steps);
void Nema17_Set_Instant_Speed(NEMA17 * nema, uint32_t velocita);
void Nema17_Enable_Step_Counter(NEMA17 * nema);
void Nema17_Do_Steps(NEMA17 * nema, uint32_t steps);
void Nema17_Reset_Steps(NEMA17 * nema);

#endif /* INC_NEMA17_H_ */
