/*
 * TMC2208.h
 *
 *  Created on: Aug 25, 2021
 *      Author: lucad
 */

#ifndef INC_TMC2208_H_
#define INC_TMC2208_H_

#include <stdbool.h>
#include "main.h"

#define TMC2208_SIZE_BUFFER_TX_RX 		8
#define TMC2208_SIZE_BUFFER_REQUEST 	4

#define HIGH							1
#define LOW								0
#define TOGGLE							2

#define BIT0							0x01
#define BIT1							0x02
#define BIT2							0x04
#define BIT3							0x08
#define BIT4							0x10
#define BIT5							0x20
#define BIT6							0x40
#define BIT7							0x80 // 1 byte
#define BIT8                            0x100
#define BIT9                            0x200
#define BIT10                           0x400
#define BIT11                           0x800
#define BIT12                           0x1000
#define BIT13                           0x2000
#define BIT14                           0x4000
#define BIT15                           0x8000 // 2 byte
#define BIT16                           0x10000
#define BIT17                           0x20000
#define BIT18                           0x40000
#define BIT19                           0x80000
#define BIT20                           0x100000
#define BIT21                           0x200000
#define BIT22                           0x400000
#define BIT23                           0x800000 // 3 byte
#define BIT24                           0x1000000
#define BIT25                           0x2000000
#define BIT26                           0x4000000
#define BIT27                           0x8000000
#define BIT28                           0x10000000
#define BIT29                           0x20000000
#define BIT30                           0x40000000
#define BIT31                           0x80000000 // 4 byte

#define TMC2208_REG_GCONF				0x00
#define TMC2208_REG_CHOPCONF			0x6C
#define TMC2208_REG_IHOLD_IRUN			0X10
#define TMC2208_REG_TPOWERDOWN			0x11
#define TMC2208_REG_PWMCONF				0x70
#define TMC2208_REG_VACTUAL				0x22
#define TMC2208_REG_MSCNT				0x6A

static const uint32_t BIT_ASSOCIATIONS[32] =
{
		BIT0, BIT1, BIT2, BIT3, BIT4, BIT5, BIT6, BIT7,
		BIT8, BIT9, BIT10, BIT11, BIT12, BIT13, BIT14, BIT15,
		BIT16, BIT17, BIT18, BIT19, BIT20, BIT21, BIT22, BIT23,
		BIT24, BIT25, BIT26, BIT27, BIT28, BIT29, BIT30, BIT31
};




typedef enum
{
	GCONF,
	CHOPCONF,
	TPOWERDOWN,
	PWMCONF,
	IHOLD_IRUN,
	VACTUAL,

	REGISTER_SIZE
} TMC2208_RegisterMap;

typedef enum
{
	I_SCALE_ANALOG,
	INTERNAL_RSENSE,
	EN_SPREADCYCLE,
	SHAFT,
	INDEX_OTPW,
	INDEX_STEP,
	PDN_DISABLE,
	MSTEP_REG_SELECT,
	MULTISTEP_FILT,
	TEST_MODE
} TMC2208_GCONF_Bits;

typedef enum
{
	IHOLD_0,
	IHOLD_1,
	IHOLD_2,
	IHOLD_3,
	IHOLD_4,

	IRUN_0,
	IRUN_1,
	IRUN_2,
	IRUN_3,
	IRUN_4,

	IHOLDDELAY_0,
	IHOLDDELAY_1,
	IHOLDDELAY_2,
	IHOLDDELAY_3
} TMC2208_IHOLD_IRUN_Bits;


typedef enum
{
	TOFF0,
	TOFF1,
	TOFF2,
	TOFF3,

	HSTRT0,
	HSTRT1,
	HSTRT2,

	HEND0,
	HEND1,
	HEND2,
	HEND3,

	CHOPCONF_RESERVED_9,
	CHOPCONF_RESERVED_8,
	CHOPCONF_RESERVED_7,
	CHOPCONF_RESERVED_6,

	TBL0,
	TBL1,

	VSENSE,

	CHOPCONF_RESERVED_5,
	CHOPCONF_RESERVED_4,
	CHOPCONF_RESERVED_3,
	CHOPCONF_RESERVED_2,
	CHOPCONF_RESERVED_1,
	CHOPCONF_RESERVED_0,

	MRES0,
	MRES1,
	MRES2,
	MRES3,

	INTPOL,
	DEDGE,
	DISS2G,
	DISS2VS
} TMC2208_CHOPCONF_Bits;

typedef enum
{
	PWM_OFS_0,
	PWM_OFS_1,
	PWM_OFS_2,
	PWM_OFS_3,
	PWM_OFS_4,
	PWM_OFS_5,
	PWM_OFS_6,
	PWM_OFS_7,

	PWM_GRAD_0,
	PWM_GRAD_1,
	PWM_GRAD_2,
	PWM_GRAD_3,
	PWM_GRAD_4,
	PWM_GRAD_5,
	PWM_GRAD_6,
	PWM_GRAD_7,

	PWM_FREQ0,
	PWM_FREQ1,

	PWM_AUTOSCALE,
	PWM_AUTOGRAD,

	FREEWHEEL0,
	FREEWHEEL1,

	PWMCONF_RESERVED_0,
	PWMCONF_RESERVED_1,

	PWM_REG_0,
	PWM_REG_1,
	PWM_REG_2,
	PWM_REG_3,

	PWM_LIM_0,
	PWM_LIM_1,
	PWM_LIM_2,
	PWM_LIM_3
} TMC2208_PWMCONF_Bits;

typedef enum
{
	UART_FREE,
	UART_WRITE,
	UART_READ,
	UART_WAIT
} TMC2208_Uart_Status;


typedef struct
{
	uint8_t tx[TMC2208_SIZE_BUFFER_TX_RX];
	uint8_t rx[TMC2208_SIZE_BUFFER_TX_RX];
	uint8_t request[TMC2208_SIZE_BUFFER_REQUEST];
} TMC2208_Buffer;

typedef struct
{
	bool rx;
} TMC2208_Interrupt;

typedef struct
{
	bool rx_received;
	TMC2208_Uart_Status uart_lock;
} TMC2208_Flag;

typedef struct
{
	TMC2208_RegisterMap name;
	uint32_t value;
	uint8_t address;
} TMC2208_Register;

typedef struct
{
	TMC2208_Register registro[REGISTER_SIZE];
} TMC2208_Registers;

typedef struct
{
	UART_HandleTypeDef * uart;
	TMC2208_Buffer buffer;
	TMC2208_Interrupt interrupt;
	TMC2208_Flag flag;
	TMC2208_Registers registri;
} TMC2208;

void TMC2208_Init(TMC2208 * tmc2208, UART_HandleTypeDef * uart); // Inizializza la struttura TMC2208
void TMC2208_CRC_Calculator(uint8_t * datagram, uint8_t datagramLength); // Calcola il crc per la comunicazione con il driver
void TMC2208_Interrupt_Read(TMC2208 * tmc2208, UART_HandleTypeDef *huart); // Lettura buffer rx dal tmc2208
void TMC2208_Read_Register(TMC2208 * tmc2208, uint8_t reg); // Invio di richiesta di lettura al driver
void TMC2208_Write_Register(TMC2208 * tmc2208, uint8_t reg); // Scrittura registro sul driver
uint32_t TMC2208_Get_Register(TMC2208 * tmc2208, uint8_t reg); // Ritorna dati del registro (fittizio)
void TMC2208_Set_Bit_Register(TMC2208 * tmc2208, uint8_t reg, uint32_t bit, uint8_t status); // imposta singolo bit di un registro
void TMC2208_Config_StealthChop(TMC2208 * tmc2208); // Configurazione tmc per modalita stealthchopper
void TMC2208_Init_Registers(TMC2208 * tmc2208); // Inizializzazione registri tmc (fittizzi)
void TMC2208_Set_Register(TMC2208 * tmc2208, uint8_t reg, uint32_t data);
#endif /* INC_TMC2208_H_ */
