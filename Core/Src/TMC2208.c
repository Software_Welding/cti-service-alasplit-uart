#include "TMC2208.h"

void TMC2208_Init(TMC2208 * tmc2208, UART_HandleTypeDef * uart)
{
	uint8_t i;

	tmc2208->uart = uart;

	for (i = 0; i < TMC2208_SIZE_BUFFER_TX_RX; i ++)
	{
		tmc2208->buffer.rx[i] = 0;
		tmc2208->buffer.tx[i] = 0;
	}

	for (i = 0; i < TMC2208_SIZE_BUFFER_REQUEST; i ++)
	{
		tmc2208->buffer.request[i] = 0;
	}

	tmc2208->interrupt.rx = false;
	tmc2208->flag.rx_received = false;
	tmc2208->flag.uart_lock = UART_FREE;

	TMC2208_Init_Registers(tmc2208);
	TMC2208_Config_StealthChop(tmc2208);
}

void TMC2208_Init_Registers(TMC2208 * tmc2208)
{
	// Registro di configurazione
	tmc2208->registri.registro[GCONF].name = GCONF;
	tmc2208->registri.registro[GCONF].value = 0x00;
	tmc2208->registri.registro[GCONF].address = 0x00;

	// Registro di configurazione chopper
	tmc2208->registri.registro[CHOPCONF].name = CHOPCONF;
	tmc2208->registri.registro[CHOPCONF].value = 0x00;
	tmc2208->registri.registro[CHOPCONF].address = 0x6C;

	// Registro di rilascio corrente motore
	tmc2208->registri.registro[TPOWERDOWN].name = TPOWERDOWN;
	tmc2208->registri.registro[TPOWERDOWN].value = 0x00;
	tmc2208->registri.registro[TPOWERDOWN].address = 0x11;

	// Registro di configurazione pwm
	tmc2208->registri.registro[PWMCONF].name = PWMCONF;
	tmc2208->registri.registro[PWMCONF].value = 0x00;
	tmc2208->registri.registro[PWMCONF].address = 0x70;

	// Registro di configurazione di corrente stallo / velocita
	tmc2208->registri.registro[IHOLD_IRUN].name = IHOLD_IRUN;
	tmc2208->registri.registro[IHOLD_IRUN].value = 0x00;
	tmc2208->registri.registro[IHOLD_IRUN].address = 0x10;

	// Registro di velocita motore
	tmc2208->registri.registro[VACTUAL].name = VACTUAL;
	tmc2208->registri.registro[VACTUAL].value = 0x00;
	tmc2208->registri.registro[VACTUAL].address = 0x22;
}

void TMC2208_Config_StealthChop(TMC2208 * tmc2208)
{
	// Controllare le configurazioni -> dovrebbe gia funzionare
	TMC2208_Set_Register(tmc2208, GCONF, 0xE0);
	  TMC2208_Write_Register(tmc2208, GCONF);

	TMC2208_Set_Register(tmc2208, CHOPCONF, 0x11010045);
	  TMC2208_Write_Register(tmc2208, CHOPCONF);

	TMC2208_Set_Register(tmc2208, IHOLD_IRUN, 0x10F6);
	  TMC2208_Write_Register(tmc2208, IHOLD_IRUN);

	TMC2208_Set_Register(tmc2208, PWMCONF, 0xCF040024);
	  TMC2208_Write_Register(tmc2208, PWMCONF);

	TMC2208_Set_Register(tmc2208, VACTUAL, 0x00);
	  TMC2208_Write_Register(tmc2208, VACTUAL);

}

void TMC2208_Set_Bit_Register(TMC2208 * tmc2208, uint8_t reg, uint32_t bit, uint8_t status)
{
	switch (status)
	{
		case LOW:
			tmc2208->registri.registro[reg].value &=~ BIT_ASSOCIATIONS[bit];
			break;

		case HIGH:
			tmc2208->registri.registro[reg].value |= BIT_ASSOCIATIONS[bit];
			break;

		case TOGGLE:
			tmc2208->registri.registro[reg].value ^= BIT_ASSOCIATIONS[bit];
			break;
	}
}

void TMC2208_Set_Register(TMC2208 * tmc2208, uint8_t reg, uint32_t data)
{
	tmc2208->registri.registro[reg].value = data;
}

uint32_t TMC2208_Get_Register(TMC2208 * tmc2208, uint8_t reg)
{
	return tmc2208->registri.registro[reg].value;
}

void TMC2208_Write_Register(TMC2208 * tmc2208, uint8_t reg)
{
	while(tmc2208->flag.uart_lock != UART_FREE)
	{
		__NOP();
	}

	tmc2208->flag.uart_lock = UART_WRITE;

	tmc2208->buffer.tx[0] = 0x05;
	tmc2208->buffer.tx[1] = 0x00;
	tmc2208->buffer.tx[2] = tmc2208->registri.registro[reg].address + 0x80;

	tmc2208->buffer.tx[3] = ((tmc2208->registri.registro[reg].value & 0xFF000000) >> 24);
	tmc2208->buffer.tx[4] = (tmc2208->registri.registro[reg].value & 0x00FF0000) >> 16;
	tmc2208->buffer.tx[5] = (tmc2208->registri.registro[reg].value & 0x0000FF00) >> 8;
	tmc2208->buffer.tx[6] = (tmc2208->registri.registro[reg].value & 0xFF);

	TMC2208_CRC_Calculator(tmc2208->buffer.tx, TMC2208_SIZE_BUFFER_TX_RX);

	HAL_UART_Transmit_IT(tmc2208->uart, tmc2208->buffer.tx, TMC2208_SIZE_BUFFER_TX_RX);
	__NOP();
	tmc2208->flag.uart_lock = UART_FREE;

	uint8_t delay = 0;
	for (delay = 0; delay < 200; delay ++)
	{
		__NOP();
	}
}

void TMC2208_Read_Register(TMC2208 * tmc2208, uint8_t reg)
{
	while(tmc2208->flag.uart_lock != UART_FREE)
	{
		__NOP();
	}

	tmc2208->flag.uart_lock = UART_WRITE;

	tmc2208->buffer.request[0] = 0x05; //sync
	tmc2208->buffer.request[1] = 0x00; //address
	tmc2208->buffer.request[2] = tmc2208->registri.registro[reg].address; //registro

	TMC2208_CRC_Calculator(tmc2208->buffer.request, TMC2208_SIZE_BUFFER_REQUEST);

	HAL_UART_Transmit_IT(tmc2208->uart, tmc2208->buffer.request, TMC2208_SIZE_BUFFER_REQUEST);
	tmc2208->interrupt.rx = true;
}

void TMC2208_Interrupt_Read(TMC2208 * tmc2208, UART_HandleTypeDef *huart)
{
	if (huart->Instance == tmc2208->uart->Instance)
	{
		if (tmc2208->interrupt.rx == true)
		{
			HAL_UART_Receive_IT(tmc2208->uart, tmc2208->buffer.rx, TMC2208_SIZE_BUFFER_TX_RX);

			uint8_t crc_ricevuto = tmc2208->buffer.rx[7];
			TMC2208_CRC_Calculator(tmc2208->buffer.rx, TMC2208_SIZE_BUFFER_REQUEST);
			uint8_t crc_calcolato = tmc2208->buffer.rx[7];
			if (crc_ricevuto == crc_calcolato)
			{
				uint8_t i;
				for (i = 0; i < TMC2208_SIZE_BUFFER_TX_RX; i ++)
				{
					if (tmc2208->registri.registro[i].address == tmc2208->buffer.rx[2])
					{
						uint32_t data = tmc2208->buffer.rx[3];
						data = data << 8;
						data |= tmc2208->buffer.rx[4];
						data = data << 8;
						data |= tmc2208->buffer.rx[5];
						data = data << 8;
						data |= tmc2208->buffer.rx[6];

						TMC2208_Set_Register(tmc2208, tmc2208->registri.registro[i].name, data);
					}
				}
			}
			tmc2208->interrupt.rx = false;
			tmc2208->flag.uart_lock = UART_FREE;
		}
	}
}

void TMC2208_CRC_Calculator(uint8_t * datagram, uint8_t datagramLength)
{
	uint8_t i,j;
	uint8_t* crc = datagram + (datagramLength-1);
	uint8_t currentByte;
	*crc = 0;
	for (i=0; i<(datagramLength-1); i++)
	{
		currentByte = datagram[i];
		for (j=0; j<8; j++)
		{
			if ((*crc >> 7) ^ (currentByte&0x01))
			{
				*crc = (*crc << 1) ^ 0x07;
			}
			else
			{
				*crc = (*crc << 1);
			}
			currentByte = currentByte >> 1;
		}
	}
}

