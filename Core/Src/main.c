/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include "NEMA17.h"
#include "TMC2208.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DEBUG_CTI_SERVICE			// apertura e chisura ogni 2 secondi in loop

#define NUMERO_PASSI_APERTURA_COMPLETA	21800 //200 step = 1mm
#define VELOCITA_MASSIMA_APERTURA		1000 // 2khz
#define VELOCITA_MASSIMA_HOMING			1000

#define REG_GCONF			0x00
#define REG_CHOPCONF		0x6C
#define REG_IHOLD_IRUN		0X10
#define REG_TPOWERDOWN		0x11
#define REG_PWMCONF			0x70
#define REG_VACTUAL			0x22
#define REG_MSCNT			0x6A
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim17;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
NEMA17 Motore_SX;
NEMA17 Motore_DX;


typedef enum
{
	SENSORE_MAGNETICO_PRESENTE,
	SENSORE_MAGNETICO_NON_PRESENTE,
} Stati_Sensore_Magnetico;
Stati_Sensore_Magnetico Sensore_Magnetico_Stato_Precedente;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM17_Init(void);
/* USER CODE BEGIN PFP */

Stati_Sensore_Magnetico STATO_SENSORE_MAGNETICO(void);
void Homing(void);
void Apertura(void);
void Release_Motori(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM17_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  HAL_Delay(200);
  HAL_TIM_PWM_Start_IT(&htim2, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start_IT(&htim3, TIM_CHANNEL_1);

  // Configurazione timer a 1khz = 1ms

  htim17.Init.Period = ((8000000  / (htim17.Init.Prescaler + 1)) / 1000); //8mhz - 1khz
  htim17.Instance -> ARR = htim17.Init.Period;
  htim17.Instance -> CCR1 = (htim17.Init.Period + 1) / 2;
  htim17.Instance -> CNT = 0;

  Nema17_Init(&Motore_SX, &huart1, GPIOB, TMC2208_ENABLE_Pin, GPIOA, MOTORE_2_DIR_Pin, FINECORSA_SX_Pin, &htim2, true, 8000000, &htim2.Instance->CCR2);
  Nema17_Init(&Motore_DX, &huart2, GPIOB, TMC2208_ENABLE_Pin, GPIOA, MOTORE_1_DIR_Pin, FINECORSA_DX_Pin, &htim3, true, 8000000, &htim3.Instance->CCR1);

  //Nema17_Enable(&Motore_SX);
  //Nema17_Enable(&Motore_DX);

  //Nema17_Enable_Step_Counter(&Motore_SX);
  //Nema17_Enable_Step_Counter(&Motore_DX);
  HAL_TIM_Base_Start_IT(&htim17);

  Homing();
  HAL_Delay(1000);

  #ifdef DEBUG_CTI_SERVICE
  while (1)
  {
	  Homing();
	  Release_Motori();
	  HAL_Delay(2000);
	  Apertura();
	  Release_Motori();
	  HAL_Delay(2000);
  }
  #endif

  Sensore_Magnetico_Stato_Precedente = STATO_SENSORE_MAGNETICO();
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  if (STATO_SENSORE_MAGNETICO() == SENSORE_MAGNETICO_PRESENTE)
	  {
		  // Se il sensore:
		  // Attualmente presente
		  // precedentemente non presente
		  if (Sensore_Magnetico_Stato_Precedente == SENSORE_MAGNETICO_NON_PRESENTE)
		  {
			  //chiudo
			  Homing();
			  Sensore_Magnetico_Stato_Precedente = SENSORE_MAGNETICO_PRESENTE;
			  Release_Motori();
		  }
	  }
	  else
	  {
		  // Se il sensore:
		  // Attualmente non presente
		  // precedentemente presente
		  if (Sensore_Magnetico_Stato_Precedente == SENSORE_MAGNETICO_PRESENTE)
		  {
			  // Apro
			  Apertura();
			  Sensore_Magnetico_Stato_Precedente = SENSORE_MAGNETICO_NON_PRESENTE;
			  Release_Motori();
		  }
	  }
	  HAL_Delay(1);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 600;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM17 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM17_Init(void)
{

  /* USER CODE BEGIN TIM17_Init 0 */

  /* USER CODE END TIM17_Init 0 */

  /* USER CODE BEGIN TIM17_Init 1 */

  /* USER CODE END TIM17_Init 1 */
  htim17.Instance = TIM17;
  htim17.Init.Prescaler = 0;
  htim17.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim17.Init.Period = 65535;
  htim17.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim17.Init.RepetitionCounter = 0;
  htim17.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim17) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM17_Init 2 */

  /* USER CODE END TIM17_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(TMC2208_ENABLE_GPIO_Port, TMC2208_ENABLE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, MOTORE_2_DIR_Pin|MOTORE_1_DIR_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : FINECORSA_DX_Pin FINECORSA_SX_Pin */
  GPIO_InitStruct.Pin = FINECORSA_DX_Pin|FINECORSA_SX_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : TMC2208_ENABLE_Pin */
  GPIO_InitStruct.Pin = TMC2208_ENABLE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(TMC2208_ENABLE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PASSI_MOTORE_1_Pin */
  GPIO_InitStruct.Pin = PASSI_MOTORE_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(PASSI_MOTORE_1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : MOTORE_2_DIR_Pin MOTORE_1_DIR_Pin */
  GPIO_InitStruct.Pin = MOTORE_2_DIR_Pin|MOTORE_1_DIR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : SENSORE_MAGNETICO_Pin */
  GPIO_InitStruct.Pin = SENSORE_MAGNETICO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SENSORE_MAGNETICO_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PASSI_MOTORE_2_Pin */
  GPIO_InitStruct.Pin = PASSI_MOTORE_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(PASSI_MOTORE_2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);

  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	Nema17_Increment_Steps(&Motore_SX, htim);
	Nema17_Increment_Steps(&Motore_DX, htim);
}
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	// Procedura homing
	Nema17_EndStop(&Motore_SX, GPIO_Pin);
	Nema17_EndStop(&Motore_DX, GPIO_Pin);
}

void Release_Motori(void)
{
	Nema17_Release(&Motore_SX);
	Nema17_Release(&Motore_DX);
}

/*
 * Nella procedura di homing i motorini devono essere sincronizzati, per questo non posso usare le procedure della libreria
 */
void Homing(void)
{
	// Mando avanti entrambi i motorini per 1 secondo
	Nema17_Start_Homing(&Motore_SX);
	Nema17_Start_Homing(&Motore_DX);

	uint32_t Velocita;
	for (Velocita = 500; Velocita < VELOCITA_MASSIMA_APERTURA; Velocita+=50)
	{
		Nema17_Set_Instant_Speed(&Motore_SX, Velocita);
		Nema17_Set_Instant_Speed(&Motore_DX, Velocita);
		HAL_Delay(10);
	}
	HAL_Delay(500);

	// Inverto senso di rotazione e attendo interrupt (incrementa contatore)
	Nema17_Homing_Stage_1(&Motore_SX);
	Nema17_Homing_Stage_1(&Motore_DX);
	// Attesa primo click
	while (Motore_SX.Min_EndStop.EndStop_Click < 1)
	{
		__NOP();
	}
	//Motore_SX.Min_EndStop.EndStop_Click = 2;
	while (Motore_DX.Min_EndStop.EndStop_Click < 1)
	{
		__NOP();
	}
	//Motore_DX.Min_EndStop.EndStop_Click = 2;

	Nema17_Set_Instant_Speed(&Motore_SX, 0);
	Nema17_Set_Instant_Speed(&Motore_DX, 0);

	Motore_DX.Min_EndStop.EndStop_Click = 0;
	Motore_SX.Min_EndStop.EndStop_Click = 0;

	HAL_Delay(1000);

	// Mando avanti i motori per mezzo secondo
	Nema17_Homing_Stage_2(&Motore_SX);
	Nema17_Homing_Stage_2(&Motore_DX);
	HAL_Delay(2000);

	// Inverto rotazione motore attendo interrupt
	Nema17_Homing_Stage_3(&Motore_SX);
	Nema17_Homing_Stage_3(&Motore_DX);

	// Attesa secondo click
	Motore_DX.Min_EndStop.EndStop_Click = 0;
	Motore_SX.Min_EndStop.EndStop_Click = 0;
	while (Motore_DX.Min_EndStop.EndStop_Click < 1)
	{
		__NOP();
	}
	//Motore_SX.Min_EndStop.EndStop_Click = 2;
	while (Motore_SX.Min_EndStop.EndStop_Click < 1)
	{
		__NOP();
	}
	Nema17_Set_Instant_Speed(&Motore_SX, 0);
	Nema17_Set_Instant_Speed(&Motore_DX, 0);
	Motore_DX.Min_EndStop.EndStop_Click = 1;
	Motore_SX.Min_EndStop.EndStop_Click = 1;

	HAL_Delay(100);

	//Nema17_Release(&Motore_SX);
	//Nema17_Release(&Motore_DX);
	// Procedura homing completata
}

void Apertura(void)
{
	Nema17_Direction_CW(&Motore_SX);
	Nema17_Direction_CW(&Motore_DX);

	Nema17_Reset_Steps(&Motore_SX);
	Nema17_Reset_Steps(&Motore_DX);

	Nema17_Enable(&Motore_SX);
	Nema17_Enable(&Motore_DX);

	Nema17_Enable_Step_Counter(&Motore_SX);
	Nema17_Enable_Step_Counter(&Motore_DX);

	Nema17_Setup_Steps_Stop(&Motore_SX, NUMERO_PASSI_APERTURA_COMPLETA);
	Nema17_Setup_Steps_Stop(&Motore_DX, NUMERO_PASSI_APERTURA_COMPLETA);
	uint32_t Velocita;
	for (Velocita = 500; Velocita < VELOCITA_MASSIMA_APERTURA; Velocita+=50)
	{
		Nema17_Set_Instant_Speed(&Motore_SX, Velocita);
		Nema17_Set_Instant_Speed(&Motore_DX, Velocita);
		HAL_Delay(10);
	}

	while ((Motore_SX.Steps < NUMERO_PASSI_APERTURA_COMPLETA) && (Motore_DX.Steps <NUMERO_PASSI_APERTURA_COMPLETA))
	{
		__NOP();
	}
}

Stati_Sensore_Magnetico STATO_SENSORE_MAGNETICO(void)
{
	if (HAL_GPIO_ReadPin(SENSORE_MAGNETICO_GPIO_Port, SENSORE_MAGNETICO_Pin) == GPIO_PIN_SET)
	{
		return SENSORE_MAGNETICO_PRESENTE;
	}
	return SENSORE_MAGNETICO_NON_PRESENTE;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
