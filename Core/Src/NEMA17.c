/*
 * NEMA17.c
 *
 *  Created on: 30 ago 2021
 *      Author: lucad
 */
#include "NEMA17.h"

void Nema17_Init(NEMA17 * nema, UART_HandleTypeDef * uart, uint32_t Enable_Pin_Port, uint32_t Enable_Pin, uint32_t Direction_Pin_Port, uint32_t Direction_Pin, uint16_t EXTI_Int_EndStop_Pin, TIM_HandleTypeDef * Timer, bool Speed_By_Timer, uint32_t Timer_Max_Frequency, uint32_t * CCR)
{
	nema->is_enable = false;

	nema->Enable.port = Enable_Pin_Port;
	nema->Enable.pin = Enable_Pin;
	nema->Dir_Pin.port = Direction_Pin_Port;
	nema->Dir_Pin.pin = Direction_Pin;
	nema->EXTI_Int_EndStop_Pin = EXTI_Int_EndStop_Pin;

	nema->Steps = 0;
	nema->Velocita = 0;

	nema->Direction = SENSE_CW;
	Nema17_Direction_CW(nema);

	nema->Timer = Timer;
	nema->Speed_By_Timer = Speed_By_Timer;
	nema->Timer_Max_Frequency = Timer_Max_Frequency;
	nema->CCR = CCR;

	nema->enable_step_counter = false;

	TMC2208_Init(&nema->Driver, uart);
}

void Nema17_Direction_CW(NEMA17 * nema)
{
	nema->Direction = SENSE_CW;
	HAL_GPIO_WritePin(nema->Dir_Pin.port, nema->Dir_Pin.pin, GPIO_PIN_SET); //sarebbe set
}

void Nema17_Direction_CCW(NEMA17 * nema)
{
	nema->Direction = SENSE_CCW;
	HAL_GPIO_WritePin(nema->Dir_Pin.port, nema->Dir_Pin.pin, GPIO_PIN_RESET); // sarebbe reset
}

void Nema17_Invert_Direction(NEMA17 * nema)
{
	uint32_t velocita = Nema17_Get_Speed(nema);

	Nema17_Set_Speed(nema, 0);

	if (Nema17_Get_Sense(nema) == SENSE_CW)
	{
		Nema17_Direction_CCW(nema);
	}
	else
	{
		Nema17_Direction_CW(nema);
	}

	Nema17_Set_Speed(nema, velocita);
}

/*
 * VELOCITA
 */
void Nema17_Set_Speed(NEMA17 * nema, uint32_t velocita)
{
	uint8_t scala = 10;
	// Aumento di velocita
	if (velocita >= nema->Velocita + scala)
	{
		for (; (nema->Velocita + scala) <= velocita; nema->Velocita+= scala)
		{
			nema->Timer->Init.Period = ((nema->Timer_Max_Frequency  / (nema->Timer->Init.Prescaler + 1)) / nema->Velocita);
			nema->Timer->Instance -> ARR = nema->Timer->Init.Period;
			*nema->CCR = (nema->Timer->Init.Period + 1) / 2;
			nema->Timer->Instance -> CNT = 0;
			HAL_Delay(1);
		}
		nema->Timer->Init.Period = ((nema->Timer_Max_Frequency  / (nema->Timer->Init.Prescaler + 1)) / velocita);
		nema->Timer->Instance -> ARR = nema->Timer->Init.Period;
		*nema->CCR = (nema->Timer->Init.Period + 1) / 2;
		nema->Timer->Instance -> CNT = 0;
		HAL_Delay(1);
	}

	// Diminuisce di velocita
	else
	{
		for (; (velocita + scala) <= nema->Velocita; nema->Velocita -= scala)
		{
			nema->Timer->Init.Period = ((nema->Timer_Max_Frequency  / (nema->Timer->Init.Prescaler + 1)) / nema->Velocita);
			nema->Timer->Instance -> ARR = nema->Timer->Init.Period;
			*nema->CCR = (nema->Timer->Init.Period + 1) / 2;
			nema->Timer->Instance -> CNT = 0;
			HAL_Delay(1);
		}
		nema->Timer->Init.Period = ((nema->Timer_Max_Frequency  / (nema->Timer->Init.Prescaler + 1)) / velocita);
		nema->Timer->Instance -> ARR = nema->Timer->Init.Period;
		*nema->CCR = (nema->Timer->Init.Period + 1) / 2;
		nema->Timer->Instance -> CNT = 0;
		HAL_Delay(1);
	}
}

void Nema17_Set_Instant_Speed(NEMA17 * nema, uint32_t velocita)
{
	nema->Timer->Init.Period = ((nema->Timer_Max_Frequency  / (nema->Timer->Init.Prescaler + 1)) / velocita);
	nema->Timer->Instance -> ARR = nema->Timer->Init.Period;
	*nema->CCR = (nema->Timer->Init.Period + 1) / 2;
	nema->Timer->Instance -> CNT = 0;
}

NEMA17_Sense Nema17_Get_Sense(NEMA17 * nema)
{
	return nema->Direction;
}

uint32_t Nema17_Get_Speed(NEMA17 * nema)
{
	return nema->Velocita;
}

/*
 * STALLO E RILASCIO
 */
void Nema17_Enable(NEMA17 * nema)
{
	Nema17_Stall(nema);
}

void Nema17_Stall(NEMA17 * nema)
{
	nema->is_enable = true;
	HAL_GPIO_WritePin(nema->Enable.port, nema->Enable.pin, GPIO_PIN_RESET);
}

void Nema17_Release(NEMA17 * nema)
{
	nema->is_enable = false;
	HAL_GPIO_WritePin(nema->Enable.port, nema->Enable.pin, GPIO_PIN_SET);
}

bool Nema17_Is_Enable(NEMA17 * nema)
{
	return nema->is_enable;
}

void Nema17_Setup_Steps_Stop(NEMA17 * nema, uint32_t steps)
{
	nema->Step_Stop.Steps = steps;
	nema->Step_Stop.enable = true;
	nema->Steps = 0;
}

void Nema17_Stop_At_N_Step(NEMA17 * nema)
{
	if (nema->Step_Stop.enable == true)
	{
		if (nema->Steps >= nema->Step_Stop.Steps)
		{
			Nema17_Set_Instant_Speed(nema, 0); // fermo motore
			//Nema17_Release(nema);
		}
	}
}

void Nema17_Do_Steps(NEMA17 * nema, uint32_t steps)
{
	Nema17_Setup_Steps_Stop(nema, steps);
	while (nema->Steps < steps)
	{
		__NOP();
	}
}

void Nema17_Reset_Steps(NEMA17 * nema)
{
	nema->Steps = 0;
}
/*
 * STEPS
 */
uint32_t Nema17_Get_Steps(NEMA17 * nema)
{
	// fare lettura tramite interrupt del registro mscnt
	return nema->Steps;
}

void Nema17_Enable_Step_Counter(NEMA17 * nema)
{
	nema->enable_step_counter = true;
}
void Nema17_Disable_Step_Counter(NEMA17 * nema)
{
	nema->enable_step_counter = false;
}

bool Nema17_Get_Enable_Step_Counter(NEMA17 * nema)
{
	return nema->enable_step_counter;
}

void Nema17_Increment_Steps(NEMA17 * nema, TIM_HandleTypeDef * Timer)
{
	if (Nema17_Get_Enable_Step_Counter(nema) == true)
	{
		if (Nema17_Is_Enable(nema) == true)
		{
			if (Timer->Instance == nema->Timer->Instance)
			{
				nema->Steps ++;

				Nema17_Stop_At_N_Step(nema);
			}
		}
	}
}

void Nema17_Start_Homing(NEMA17 * nema)
{
	nema->Min_EndStop.enable = true;
	nema->Min_EndStop.EndStop_Click = 0;
	nema->Home = false;
	nema->Step_Stop.enable = false;

	Nema17_Direction_CW(nema);
	//Nema17_Set_Instant_Speed(nema, 1000);
	Nema17_Enable(nema);
	//Nema17_Do_Steps(nema, 200);
}

void Nema17_Homing_Stage_1(NEMA17 * nema)
{
	Nema17_Direction_CCW(nema);
	Nema17_Set_Instant_Speed(nema, 1000);
}

void Nema17_Homing_Stage_2(NEMA17 * nema)
{
	Nema17_Direction_CW(nema);
	Nema17_Set_Instant_Speed(nema, 500);
}

void Nema17_Homing_Stage_3(NEMA17 * nema)
{
	Nema17_Direction_CCW(nema);
	Nema17_Set_Instant_Speed(nema, 300);
}

void Nema17_EndStop(NEMA17 * nema, uint16_t GPIO_Pin)
{
	if (GPIO_Pin == nema -> EXTI_Int_EndStop_Pin)
	{
		if (nema->Min_EndStop.enable == true)
		{
			nema->Min_EndStop.EndStop_Click ++;
			if (nema->Min_EndStop.EndStop_Click == 1)
			{
				Nema17_Set_Instant_Speed(nema, 0);
			}
		}
			// Arrivato erroneamente
		//Nema17_Set_Instant_Speed(nema, 0); // fermo motore
		//Nema17_Direction_CW(nema); // inverto corsa motore
	}
}

